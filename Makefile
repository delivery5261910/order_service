
CURRENT_DIR=$(shell pwd)

run:
	go run cmd/main.go

swag-init:
	swag init -g api/router.go -o api/docs

proto-gen:
	./scripts/gen_proto.sh

submodule:
	git submodule add git@gitlab.com:delivery5261910/delivery_protos.git
