package service

import (
	"context"
	"fmt"
	"google.golang.org/protobuf/types/known/emptypb"
	pb "order_service/genproto/order_service"
	// pbu "order_service/genproto/user_service"
	"order_service/grpc/client"
	"order_service/pkg/logger"
	"order_service/storage"
)

type orderService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pb.UnimplementedOrderServiceServer
}

func NewOrderService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *orderService {
	return &orderService{
		storage:  strg,
		services: services,
		log:      log,
	}
}

func (o *orderService) Create(ctx context.Context, request *pb.CreateOrderRequest) (*pb.Order, error) {

	user, err := o.storage.Order().Create(ctx, request)
	if err != nil {
		fmt.Println("Err 1", err.Error())
		return nil, err
	}

	// client := pbu.Client{
	// 	Id: user.Client,
	// 	TotalOrdersSum: user.Price,
	// }

	// o.services.ClientService().Update(ctx, &client)

	return user, nil
}

func (o *orderService) Get(ctx context.Context, key *pb.PrimaryKey) (*pb.Order, error) {
	fmt.Println(key)
	return o.storage.Order().Get(ctx, key)
}

func (o *orderService) GetList(ctx context.Context, request *pb.GetListRequest) (*pb.OrderResponse, error) {
	return o.storage.Order().GetList(ctx, request)
}

func (o *orderService) Update(ctx context.Context, order *pb.Order) (*pb.Order, error) {
	return o.storage.Order().Update(ctx, order)
}

func (o *orderService) Delete(ctx context.Context, key *pb.PrimaryKey) (*emptypb.Empty, error) {
	return o.storage.Order().Delete(ctx, key)
}
