package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	pb "order_service/genproto/order_service"
	"order_service/grpc/client"
	"order_service/pkg/logger"
	"order_service/service"
	"order_service/storage"
)

func SetUpServer(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *grpc.Server {
	grpcServer := grpc.NewServer()

	// Order Service

	pb.RegisterOrderServiceServer(grpcServer, service.NewOrderService(strg, services, log))

	reflection.Register(grpcServer)

	return grpcServer
}
