package client

import (
  "order_service/config"
  "order_service/genproto/order_service"
  "order_service/genproto/user_service"

  "google.golang.org/grpc"
)

type IServiceManager interface {
  // Order service 

  OrderService() order_service.OrderServiceClient

  // Client service 

  ClientService() user_service.ClientServiceClient
}

type grpcClients struct {
  orderService order_service.OrderServiceClient
  clientService user_service.ClientServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
  connOrderService, err := grpc.Dial(
    cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
    grpc.WithInsecure(),
  )
  if err != nil {
    return nil, err
  }

  connUserService, err := grpc.Dial(
    cfg.ClientServiceGrpcHost+cfg.ClientServiceGrpcPort,
    grpc.WithInsecure(),
  )
  if err != nil {
    return nil, err
  }

  return &grpcClients{

    // Order service

    orderService: order_service.NewOrderServiceClient(connOrderService),

    // Client service

    clientService:  user_service.NewClientServiceClient(connUserService),
  }, nil
}

func (g *grpcClients) OrderService() order_service.OrderServiceClient {
  return g.orderService
}

func (g *grpcClients) ClientService() user_service.ClientServiceClient {
  return g.clientService
}