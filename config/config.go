package config

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/spf13/cast"
	"os"
)

type Config struct {
	PostgresHost     string
	PostgresPort     string
	PostgresUser     string
	PostgresPassword string
	PostgresDB       string

	Token   		 string

	ServiceName string
	Environment string
	LoggerLevel string

	ServiceGrpcHost string
	ServiceGrpcPort string

	ClientServiceGrpcHost string
	ClientServiceGrpcPort string

}

func Load() Config {
	if err := godotenv.Load(); err != nil {
		fmt.Println(".env not found", err)
	}

	cfg := Config{}

	cfg.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	cfg.PostgresPort = cast.ToString(getOrReturnDefault("POSTGRES_PORT", "5432"))
	cfg.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "postgres"))
	cfg.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "password"))
	cfg.PostgresDB = cast.ToString(getOrReturnDefault("POSTGRES_DB", "db"))

	cfg.Token = cast.ToString(getOrReturnDefault("TOKEN", "token"))

	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME", "user_service"))
	cfg.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "dev"))
	cfg.LoggerLevel = cast.ToString(getOrReturnDefault("LOGGER_LEVEL", "debug"))

	cfg.ServiceGrpcHost = cast.ToString(getOrReturnDefault("GRPC_HOST", "localhost"))
	cfg.ServiceGrpcPort = cast.ToString(getOrReturnDefault("GRPC_PORT", ":8002"))

	cfg.ClientServiceGrpcHost = cast.ToString(getOrReturnDefault("CLIENT	_SERVICE_GRPC_HOST", "localhost"))
	cfg.ClientServiceGrpcPort = cast.ToString(getOrReturnDefault("CLIENT	_SERVICE_GRPC_PORT", ":8080"))


	return cfg
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	value := os.Getenv(key)
	if value != "" {
		return value
	}

	return defaultValue
}
