CREATE TYPE "order_type" AS ENUM ('delivery', 'pick_up');
CREATE TYPE "order_status" AS ENUM ('accepted', 'courier_accepted', 'ready_in_branch', 'on_way', 'finished', 'canceled');
CREATE TYPE "payment_type" AS ENUM ('cash', 'card');
CREATE TYPE "delivery_tariff_type" AS ENUM ('fixed', 'alternative');

CREATE TABLE IF NOT EXISTS "orders" (
    "id" uuid PRIMARY KEY,
    "order_id" varchar(120),
    "client" varchar(70),
    "branch_id" varchar(70),
    "type" order_type,
    "address" varchar(100),
    "courier_id" varchar(70),
    "price" float,
    "delivery_price" float,
    "discount" int,
    "status" order_status DEFAULT 'accepted',
    "payment_type" payment_type,
    "searching_column" TEXT,
    "created_at" timestamp DEFAULT (now()),
    "updated_at" timestamp DEFAULT (now()),
    "deleted_at" int DEFAULT 0
);

CREATE TABLE IF NOT EXISTS "order_products" (
    "id" uuid PRIMARY KEY,
    "order_id" UUID REFERENCES orders(id),
    "product_id" UUID,
    "quantity" INT,
    "price" BIGINT,
    "searching_column" TEXT,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "deleted_at" INT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS "delivery_tarif" (
    "id" UUID PRIMARY KEY,
    "name" VARCHAR(30),
    "type" delivery_tariff_type,
    "base_price" BIGINT,
    "searching_column" TEXT,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP,
    "deleted_at" INT DEFAULT 0
);


