package postgres

import (
	"context"
	"fmt"
	pb "order_service/genproto/order_service"
	"order_service/pkg/helper"
	"order_service/pkg/logger"
	"order_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type orderRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewOrderRepo(db *pgxpool.Pool, log logger.ILogger) storage.IOrderStorage {
	return &orderRepo{
		db:  db,
		log: log,
	}
}

func (c *orderRepo) Create(ctx context.Context, request *pb.CreateOrderRequest) (*pb.Order, error) {
	var (
		order           = pb.Order{}
		err             error
		searchingColumn string
	)

	query := `
		insert into orders (id, order_id, client, branch_id, type, address, courier_id, delivery_price, discount, payment_type, searching_column)
			values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
			returning id, order_id, client, branch_id, type, address, courier_id, price, delivery_price, discount, status, payment_type`

	searchingColumn = fmt.Sprintf("%s %s %s %s %s %s %2f %2f %2f %s %s", request.OrderId, request.Client, request.BranchId, request.Type, request.Address, request.CourierId, request.Price, request.DeliveryPrice, request.Discount, request.Status, request.PaymentType)

	if err = c.db.QueryRow(ctx, query,
		uuid.New().String(),
		request.OrderId,
		request.Client,
		request.BranchId,
		request.Type,
		request.Address,
		request.CourierId,
		request.DeliveryPrice,
		request.Discount,
		request.PaymentType,
		searchingColumn).
		Scan(
			&order.Id,
			&order.OrderId,
			&order.Client,
			&order.BranchId,
			&order.Type,
			&order.Address,
			&order.CourierId,
			&order.Price,
			&order.DeliveryPrice,
			&order.Discount,
			&order.Status,
			&order.PaymentType,
		); err != nil {
		fmt.Println("err 3", err.Error())
		return nil, err
	}

	return &order, nil
}

func (c *orderRepo) Get(ctx context.Context, key *pb.PrimaryKey) (*pb.Order, error) {
	var (
		order = pb.Order{}
	)

	query := `select id, order_id, client, branch_id, type, address, courier_id, price, delivery_price, discount, status, payment_type from orders where deleted_at = 0 and id = $1`

	if err := c.db.QueryRow(ctx, query, key.GetId()).Scan(
		&order.Id,
		&order.OrderId,
		&order.Client,
		&order.BranchId,
		&order.Type,
		&order.Address,
		&order.CourierId,
		&order.Price,
		&order.DeliveryPrice,
		&order.Discount,
		&order.Status,
		&order.PaymentType,
	); err != nil {
		c.log.Error("error while scanning order by id", logger.Error(err))
		return nil, err
	}

	return &order, nil
}

func (c *orderRepo) GetList(ctx context.Context, request *pb.GetListRequest) (*pb.OrderResponse, error) {
	var (
		resp   = pb.OrderResponse{}
		filter = "where deleted_at = 0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s' ", request.GetSearch())
	}

	countQuery := `select count(1) from orders ` + filter

	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		c.log.Error("error while scanning count of orders", logger.Error(err))
		return nil, err
	}

	query := `select id, order_id, client, branch_id, type, address, courier_id, price, delivery_price, discount, status, payment_type from orders ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		c.log.Error("error while getting order rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			order = pb.Order{}
		)

		if err = rows.Scan(
			&order.Id,
			&order.OrderId,
			&order.Client,
			&order.BranchId,
			&order.Type,
			&order.Address,
			&order.CourierId,
			&order.Price,
			&order.DeliveryPrice,
			&order.Discount,
			&order.Status,
			&order.PaymentType,
		); err != nil {
			c.log.Error("error while scanning ony by one order", logger.Error(err))
			return nil, err
		}

		resp.Orders = append(resp.Orders, &order)
	}

	resp.Count = count

	return &resp, nil
}

func (c *orderRepo) Update(ctx context.Context, order *pb.Order) (*pb.Order, error) {
	var (
		resp   = pb.Order{}
		params = make(map[string]interface{})
		query  = `update orders set `
		filter = ""
	)

	params["id"] = order.GetId()

	if order.GetType() != "" {

		params["type"] = order.GetType()

		filter += " type = @type,"
	}

	if order.GetAddress() != "" {
		params["address"] = order.GetAddress()

		filter += " address = @address,"
	}

	if order.GetPrice() != 0 {
		params["price"] = order.GetPrice()

		filter += " price = @price,"
	}

	if order.GetDeliveryPrice() != 0 {
		params["delivery_price"] = order.GetDeliveryPrice()

		filter += " delivery_price = @delivery_price,"
	}

	if order.GetStatus() != "" {
		params["status"] = order.GetStatus()

		filter += " status = @status,"
	}

	if order.GetPaymentType() != "" {
		params["payment_type"] = order.GetPaymentType()

		filter += " payment_type = @payment_type,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := c.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *orderRepo) Delete(ctx context.Context, key *pb.PrimaryKey) (*emptypb.Empty, error) {
	_, err := c.db.Exec(ctx, `update orders set deleted_at = extract(epoch from current_timestamp) where id = $1`, key.GetId())

	return nil, err
}
