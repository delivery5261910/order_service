package storage

import (
	"context"
	"google.golang.org/protobuf/types/known/emptypb"
	pb "order_service/genproto/order_service"
)

type IStorage interface {
	Close()
	Order() IOrderStorage
}

type IOrderStorage interface {
	Create(context.Context, *pb.CreateOrderRequest) (*pb.Order, error)
	Get(context.Context, *pb.PrimaryKey) (*pb.Order, error)
	GetList(context.Context, *pb.GetListRequest) (*pb.OrderResponse, error)
	Update(context.Context, *pb.Order) (*pb.Order, error)
	Delete(context.Context, *pb.PrimaryKey) (*emptypb.Empty, error)
}
